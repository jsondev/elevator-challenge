# Trouble at Crowdskout HQ

Oh no! One of our employees was working out on the rooftop terrace when the elevators broke down. Someone fried all the logic that made them tick. Your job is simple: rescue the stranded developer by rewriting the missing code and bringing him safely to his destination via a functioning elevator (any other means such as helicopters, rappelling or teleportation should only be attempted once you have fixed the elevators).

The provided code is a simple step-based simulation of our HQ with some existing vanilla JavaScript 'classes'. You have total control over the code, feel free to introduce any libraries or frameworks, add any additional functionality, or refactor as you see fit. While the code is fair game, the general architecture should remain the same, a building, floors, elevators and people should all remain as distinct classes (you can certainly add more new types if necessary).

Remember: your only required goal is to save the developer. However, you do have artistic license to improve the interface, framework, or add fun features!
